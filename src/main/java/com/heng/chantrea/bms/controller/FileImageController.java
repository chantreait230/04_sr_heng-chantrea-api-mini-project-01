package com.heng.chantrea.bms.controller;

import com.heng.chantrea.bms.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("api/v1")
public class FileImageController {

    @Value(value = "${file.upload.server.path}")
    private String serverPath;

    @Value("${file.base.url}")
    private String imageUrl;

    @Autowired
    FileService fileStorageService;

    public void setFileStorageService(FileService fileStorageService) {
        this.fileStorageService = fileStorageService;
    }

    @PostMapping("/thumbnail")
    public ResponseEntity<Map<String,Object>> uploadFile(@RequestParam("file") MultipartFile file) {
        Map<String, Object> res = new HashMap<>();

        String filename = serverPath+file.getOriginalFilename() ;
        try {
            String fileName = fileStorageService.saveUser(file);
            res.put("message","Uploaded the file successfully");
            res.put("status",true);
            res.put("data",(imageUrl+fileName));
            return ResponseEntity.status(HttpStatus.OK).body(res);
        } catch (Exception e) {

            res.put("message","Could not upload the file:");
            res.put("status",false);
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(res);
        }
    }
}
