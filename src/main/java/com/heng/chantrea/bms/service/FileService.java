package com.heng.chantrea.bms.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;


public interface FileService {
    String saveUser(MultipartFile file);
    Resource load(String filename);
}
