package com.heng.chantrea.bms.configuration;

import com.heng.chantrea.bms.repository.model.Book;
import com.heng.chantrea.bms.repository.model.Category;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;

@Configuration
public class ApplicationConfiguration implements RepositoryRestConfigurer {
    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.exposeIdsFor(Book.class,Category.class);
        config.setBasePath("api/v1");
    }
}
