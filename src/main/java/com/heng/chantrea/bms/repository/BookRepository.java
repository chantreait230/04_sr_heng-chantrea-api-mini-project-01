package com.heng.chantrea.bms.repository;

import com.heng.chantrea.bms.repository.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

public interface BookRepository extends JpaRepository<Book,Long> {
    @RestResource(path = "bookTitle")
    List<Book> findByTitleContainingIgnoreCase(@Param("title") String title);
}
