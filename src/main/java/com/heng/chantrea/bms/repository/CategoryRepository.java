package com.heng.chantrea.bms.repository;

import com.heng.chantrea.bms.repository.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

public interface CategoryRepository extends JpaRepository<Category,Long> {
}
